<?php

namespace App\Services\Order\Dto;

use App\Http\Requests\Order\OrderRequest;
use Spatie\DataTransferObject\DataTransferObject;

class OrderDto extends DataTransferObject
{
    public string $phoneNumber;
    public string $fullName;
    public int $amount;
    public string $deliveryAddress;

    public static function fromRequest(OrderRequest $request): self
    {
        return new self(
            phoneNumber: $request->getPhoneNumber(),
            fullName: $request->getFullName(),
            amount: $request->getAmount(),
            deliveryAddress: $request->getDeliveryAddress()
        );
    }
}
