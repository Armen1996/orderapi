<?php

namespace App\Services\Order\Dto;

use App\Http\Requests\Order\UpdateOrderRequest;
use Spatie\DataTransferObject\DataTransferObject;

class UpdateOrderDto extends DataTransferObject
{
    public int $id;
    public OrderDto $orderDto;

    public static function fromRequest(UpdateOrderRequest $request): self
    {
        return new self(
            id: $request->getId(),
            orderDto: OrderDto::fromRequest($request)
        );
    }
}
