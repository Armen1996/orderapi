<?php

namespace App\Services\Order\Dto;

use App\Http\Requests\Order\CreateOrderRequest;
use Spatie\DataTransferObject\DataTransferObject;

class CreateOrderDto extends DataTransferObject
{
    public OrderDto $orderDto;

    public static function fromRequest(CreateOrderRequest $request): self
    {
        return new self(
            orderDto: OrderDto::fromRequest($request)
        );
    }
}
