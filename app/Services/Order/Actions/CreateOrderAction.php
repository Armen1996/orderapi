<?php

namespace App\Services\Order\Actions;

use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Services\Order\Dto\CreateOrderDto;
use App\Repositories\Write\Order\OrderWriteRepository;


class CreateOrderAction
{
    protected OrderWriteRepository $orderWriteRepository;

    public function __construct(
        OrderWriteRepository $orderWriteRepository
    ) {
        $this->orderWriteRepository = $orderWriteRepository;
    }

    public function create(CreateOrderDto $dto): OrderResource
    {
        $order = Order::create($dto);
        $order = $this->orderWriteRepository->save($order);

        return new OrderResource($order);
    }
}
