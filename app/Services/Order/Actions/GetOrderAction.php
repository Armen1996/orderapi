<?php

namespace App\Services\Order\Actions;

use App\Http\Resources\Order\OrderResource;
use App\Repositories\Read\Order\OrderReadRepository;

class GetOrderAction
{
    protected OrderReadRepository $orderReadRepository;

    public function __construct(OrderReadRepository $orderReadRepository)
    {
        $this->orderReadRepository = $orderReadRepository;
    }

    public function get(string $id): OrderResource
    {
        $order = $this->orderReadRepository->getById($id);

        return new OrderResource($order);
    }
}
