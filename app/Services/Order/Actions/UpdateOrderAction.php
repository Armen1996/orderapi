<?php

namespace App\Services\Order\Actions;

use App\Http\Resources\Order\OrderResource;
use App\Repositories\Write\Order\OrderWriteRepository;
use App\Services\Order\Dto\UpdateOrderDto;
use App\Repositories\Read\Order\OrderReadRepository;

class UpdateOrderAction
{
    protected OrderReadRepository $orderReadRepository;
    protected OrderWriteRepository $orderWriteRepository;

    public function __construct(
        OrderReadRepository $orderReadRepository,
        OrderWriteRepository $orderWriteRepository
    ) {
        $this->orderReadRepository = $orderReadRepository;
        $this->orderWriteRepository = $orderWriteRepository;
    }

    public function update(UpdateOrderDto $dto): OrderResource
    {
        $order = $this->orderReadRepository->getById($dto->id);

        $order->updateByApi($dto);
        $order = $this->orderWriteRepository->save($order);

        return new OrderResource($order);
    }
}
