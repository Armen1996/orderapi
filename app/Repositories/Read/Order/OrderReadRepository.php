<?php

namespace App\Repositories\Read\Order;

use App\Models\Order;
use App\Exceptions\OrderDoesNotExistException;
use Illuminate\Database\Eloquent\Builder;

class OrderReadRepository implements OrderReadRepositoryInterface
{
    private function query(): Builder
    {
        return Order::query();
    }

    public function getById(int $id): Order
    {
        $order = $this->query()
            ->where('id', $id)
            ->first();

        if (is_null($order)) {
            throw new OrderDoesNotExistException();
        }

        return $order;
    }
}
