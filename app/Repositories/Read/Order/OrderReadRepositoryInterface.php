<?php

namespace App\Repositories\Read\Order;

use App\Models\Order;

interface OrderReadRepositoryInterface
{
    public function getById(int $id): Order;
}
