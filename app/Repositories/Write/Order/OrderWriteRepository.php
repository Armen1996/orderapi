<?php

namespace App\Repositories\Write\Order;

use App\Models\Order;
use App\Exceptions\SavingErrorException;

class OrderWriteRepository implements OrderWriteRepositoryInterface
{
    public function save(Order $order): Order
    {
        if (!$order->save()) {
            throw new SavingErrorException();
        }

        return $order;
    }
}
