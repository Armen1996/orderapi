<?php

namespace App\Models;

use App\Services\Order\Dto\CreateOrderDto;
use App\Services\Order\Dto\UpdateOrderDto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $phone_number
 * @property string full_name
 * @property int $amount
 * @property string $delivery_address
 */
class Order extends Model
{
    use HasFactory;

    public static function create(CreateOrderDto $dto): self
    {
        $order = new self();

        $order->setPhoneNumber($dto->orderDto->phoneNumber);
        $order->setFullName($dto->orderDto->fullName);
        $order->setAmount($dto->orderDto->amount);
        $order->setDeliveryAddress($dto->orderDto->deliveryAddress);

        return $order;
    }

    public function updateByApi(UpdateOrderDto $dto): void
    {
        $this->setPhoneNumber($dto->orderDto->phoneNumber);
        $this->setFullName($dto->orderDto->fullName);
        $this->setAmount($dto->orderDto->amount);
        $this->setDeliveryAddress($dto->orderDto->deliveryAddress);
    }

    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phone_number = $phoneNumber;
    }

    public function setFullName(string $fullName): void
    {
        $this->full_name = $fullName;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function setDeliveryAddress(string $deliveryAddress): void
    {
        $this->delivery_address = $deliveryAddress;
    }
}
