<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'phoneNumber' => $this->resource->phone_number,
            'fullName' => $this->resource->full_name,
            'amount' => $this->resource->amount,
            'deliveryAddress' => $this->resource->delivery_address,
        ];
    }
}
