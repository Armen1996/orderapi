<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class GetOrderRequest extends FormRequest
{
    const ID = 'id';

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            self::ID => [
                'integer',
            ],
        ];
    }

    public function getId(): string
    {
        return (int)$this->route(self::ID);
    }
}
