<?php

namespace App\Http\Requests\Order;

class UpdateOrderRequest extends OrderRequest
{
    const ID = 'id';

    public function rules(): array
    {
        return array_merge(parent::rules(), [
            self::ID => [
                'integer',
            ],
        ]);
    }

    public function getId(): int
    {
        return (int)$this->route(self::ID);
    }
}
