<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    const PHONE_NUMBER = 'phoneNumber';
    const FULL_NAME = 'fullName';
    const AMOUNT = 'amount';
    const DELIVERY_ADDRESS = 'deliveryAddress';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            self::PHONE_NUMBER => [
                'required',
                'string'
            ],
            self::FULL_NAME => [
                'required',
                'string'
            ],
            self::AMOUNT => [
                'required',
                'integer'
            ],
            self::DELIVERY_ADDRESS => [
                'required',
                'string'
            ],
        ];
    }

    public function getPhoneNumber(): string
    {
        return $this->get(self::PHONE_NUMBER);
    }

    public function getFullName(): string
    {
        return $this->get(self::FULL_NAME);
    }

    public function getAmount(): string
    {
        return $this->get(self::AMOUNT);
    }

    public function getDeliveryAddress(): string
    {
        return $this->get(self::DELIVERY_ADDRESS);
    }
}
