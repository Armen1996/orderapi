<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Http\Requests\Order\GetOrderRequest;
use App\Services\Order\Actions\CreateOrderAction;
use App\Services\Order\Actions\UpdateOrderAction;
use App\Services\Order\Actions\GetOrderAction;
use App\Services\Order\Dto\CreateOrderDto;
use App\Services\Order\Dto\UpdateOrderDto;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    protected CreateOrderAction $createOrderAction;
    protected UpdateOrderAction $updateOrderAction;
    protected GetOrderAction $getOrderAction;

    public function __construct(
        CreateOrderAction $createOrderAction,
        UpdateOrderAction $updateOrderAction,
        GetOrderAction $getOrderAction
    ) {
        $this->createOrderAction = $createOrderAction;
        $this->updateOrderAction = $updateOrderAction;
        $this->getOrderAction = $getOrderAction;
    }

    public function create(CreateOrderRequest $request): JsonResponse
    {
        $dto = CreateOrderDto::fromRequest($request);

        $result = $this->createOrderAction->create($dto);

        return $this->response($result->toArray($request), statusCode: JsonResponse::HTTP_CREATED);
    }

    public function update(UpdateOrderRequest $request): JsonResponse
    {
        $dto = UpdateOrderDto::fromRequest($request);

        $result = $this->updateOrderAction->update($dto);

        return $this->response($result->toArray($request));
    }

    public function get(GetOrderRequest $request): JsonResponse
    {
        $result = $this->getOrderAction->get($request->getId());

        return $this->response($result->toArray($request));
    }
}
