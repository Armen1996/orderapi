<?php

namespace App\Exceptions;

class OrderDoesNotExistException extends BusinessLogicException
{
    public function getStatus(): int
    {
        return BusinessLogicException::ORDER_DOES_NOT_EXIST;
    }

    public function getStatusMessage(): string
    {
        return __('errors.order_does_not_exist');
    }
}
